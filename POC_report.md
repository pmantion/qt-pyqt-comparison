# [Py]Qt POC array editor GUI

**Objective:**
Build a GUI application to showcase a widget to edit large 2D arrays and compare the same in Qt(C++) and PyQt.

Among the available views/widgets in Qt it was evident that the `TableWidget` or `TableView` implementations would be an appropriate choice to display 2D arrays.

For high level abstraction purpose, the following two libraries were used to generate the said array:
  * Numpy for PyQt implementation, and
  * Xtensor for Qt implementation.

*Xtensor was used to maintain the familiarity with the numpy syntax.* 

Another benchmark was made using vanilla arrays.

## TableWidget Approach

The most simple approach would be to implement a `TableWidget` which will be responsible for rendering the said array. In order to populate `TableWidget` cells with a data value one has to use the `setItem` and incase of a widget one must use `setCellWidget` method of the `TableWidget` instance.

The `setItem` method has the following signature:

`QTableWidget.setItem (self, int row, int column, QTableWidgetItem item)`

The `setCellWidget` widget also has a similar signature, except it requires a `QWidget` instance instead of a `QTableWidgetItem` one.

From the signature it is evident that we need to explicitly set the individual data cells in order to achieve our objective.

### Observation

The respective implementations are tested against arrays of different sizes containing `double` values.


|    Array Size  |  Time Taken (Qt)  |  Time Taken (PyQt) |
|----------------|-------------------|--------------------|
|   1000 x 1000  |       0.568       |      3.608         |
|   2500 x 2500  |       3.632       |      24.614        |
|   5000 x 5000  |       15.307      |      94.792        |

*Timings are in seconds.*

### Inference

* Time taken to render the arrays are very high and is not acceptable based on the array sizes that are usually handled on a day to day basis.
* Qt is quite faster than the PyQt implementation.
* Both the implementations were quite simple to implement yet the PyQt implementation was simpler.

### Possible Reasons

* The `Widget` approach is slower due to the fact that `setItem` tries to refresh the view each time a value is assigned to a cell. Therefore, a previously assigned cell is being refreshed unnecessarily each time a different cell is assigned a new value. The time measured in this experiment aren't acceptable for GUI: independently of the language the proper widget/abstraction needs to be used.
* PyQt is comparatively slower due to frequent switching of python and C++ APIs.


## TableView Approach
A different approach to solve this problem would be to utilize the [Model-View](https://doc.qt.io/qt-5/model-view-programming.html) architecture provided by Qt. The idea is to handle data(Model) and view separately.

> Qt contains a set of item view classes that use a model/view architecture to manage the relationship between data and the way it is presented to the user. 

>The separation of functionality introduced by this architecture gives developers greater flexibility to customize the presentation of items, and provides a standard model interface to allow a wide range of data sources to be used with existing item views.

In our Widget approach the Model and View were bound together by default as standalone widget.

Qt has provided us helpful abstract classes to implement the same. For `TableView` we can inherit the `QAbstractTableModel` and override it's methods to effeciently implement our solution.

### Observation

|   Array Size  |   Time Taken (Qt) |   Time Taken (PyQt) |
|---------------|-------------------|---------------------|
|  1000 x 1000  |       0.015       |        0.012        |
|  5000 x 5000  |       0.162       |        0.008        |
| 10000 x 10000 |       0.563       |        0.009        |
| 20000 x 20000 |       1.915       |        0.011        |

*Timings are in seconds*

### Inference

* Time taken to render the arrays are considerably less compared to the previous `TableWidget` approach: The choice of widget is more adapted to the amount of data. The observed time does not depend on the size of the data.
* Surprisingly, Qt(C++) is thousands of magnitude slower than the PyQt implementation. It is also observed that time taken to render the array is growing consistently with the size of the array.
* Again, both the implementations were quite similar but it takes longer to implement in Qt due to it's syntax.

### Possible Reasons

* Majority of the time must be spent on copying and modifying the underlying memory.
* As xtensor is responsible for handling the arrays. It might be responsible for the slowdown.

## Vanilla Array Approach

In order to understand the influence of xtensor and numpy on render timings, we implement the `TableView` using native data structures like lists in python and vectors in C++.

### Observation

|   Array Size  |   Time Taken (Qt) |   Time Taken (PyQt) |
|---------------|-------------------|---------------------|
|  1000 x 1000  |       0.003       |        0.002        |
|  5000 x 5000  |       0.005       |        0.046        |
| 10000 x 10000 |       0.022       |        0.192        |
| 20000 x 20000 |       0.154       |        0.875        |

*Timings are in seconds*

### Inference

* Difference in time taken in both the implementations are quite close with respect to the enormously large array sizes (40,000,000 float data points).
* C++ is slightly faster than python as expected. In context of a GUI the difference is not significant.

## Final Conclusion

* `Qt` and `PyQt` both are **fast** enough to handle and render arrays of large sizes (Around 320MB of data).
* C++ is slightly **faster** than Python due to its easy access to memory and pointer manipulation. Although we saw that it depends on the data structure implementation.
* Python development is **quicker** than C++ and it also has a smaller codebase.


As Python has support for type hints and requires less time to code than C++, despite of having negligible performance degradation, `PyQt`can be considered as a viable solution.

> Disclaimer: The measured time and the measurement technique do not completely match our assumptions (C++ is faster than Python and passing a reference should not depend on the size of the data). However, the perceived time felt responsive enough for a GUI.
