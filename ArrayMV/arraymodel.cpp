#include "arraymodel.h"
#include <xtensor/xarray.hpp>

ArrayModel::ArrayModel(QObject *parent, xt::xarray<double> array)
    : QAbstractTableModel(parent)
{
    data_array = array;
}

int ArrayModel::rowCount(const QModelIndex &parent) const
{
    return data_array.shape(0);
}

int ArrayModel::columnCount(const QModelIndex &parent) const
{
    return data_array.shape(1);
}

QVariant ArrayModel::data(const QModelIndex &index, int role) const
{
    if(role == Qt::DisplayRole)
        return QString::number(data_array(index.row(), index.column()));
    return QVariant();
}

QVariant ArrayModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(role != Qt::DisplayRole)
        return QVariant();

    if(role == Qt::DisplayRole)
    {
        if(orientation == Qt::Horizontal)
            return QString::fromStdString("Column ") + QString::number(section);
        else
            return QString::fromStdString("Row ") + QString::number(section);
    }

    return QVariant();
}

Qt::ItemFlags ArrayModel::flags(const QModelIndex &index) const
{
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

bool ArrayModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if(role == Qt::EditRole){
            data_array(index.row(), index.column()) = value.toDouble();
            return true;
        }

    return false;
}

bool ArrayModel::setArray(xt::xarray<double> array)
{
    beginResetModel();
    data_array = array;
    endResetModel();
    return true;
}
