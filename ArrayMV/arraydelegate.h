#ifndef ARRAYDELEGATE_H
#define ARRAYDELEGATE_H
#include <QStyledItemDelegate>

class ArrayDelegate : public QStyledItemDelegate
{
    Q_OBJECT
public:
    ArrayDelegate(QObject *parent=nullptr);
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const override;
};

#endif // ARRAYDELEGATE_H
