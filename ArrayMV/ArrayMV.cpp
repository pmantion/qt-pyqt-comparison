#include "ArrayMV.h"
#include "ui_ArrayMV.h"
#include <xtensor/xarray.hpp>
#include <xtensor/xrandom.hpp>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tableView->setModel(&model);
    ui->tableView->setItemDelegate(&delegate);
    ui->tableView->setWordWrap(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_loadButton_clicked()
{
    size_t rows = ui->rowBox->value();
    size_t cols = ui->colBox->value();
    xt::xarray<double> array;

    auto data_t_0 = std::chrono::high_resolution_clock::now();
    try
    {
        array = xt::random::rand<double>({rows, cols});
    }
    catch(std::bad_alloc & e)
    {
        ui->statusbar->showMessage(QString::fromStdString(e.what()));
    }
    auto data_t_1 = std::chrono::high_resolution_clock::now();

    auto render_t_0 = std::chrono::high_resolution_clock::now();
    model.setArray(array);
    auto render_t_1 = std::chrono::high_resolution_clock::now();

    auto data_time_taken = std::chrono::duration<double>(data_t_1 - data_t_0);
    auto render_time_taken = std::chrono::duration<double>(render_t_1 - render_t_0);

    ui->memoryLabel->setText(QString::number(data_time_taken.count()) + QString::fromStdString(" s"));
    ui->renderLabel->setText(QString::number(render_time_taken.count()) + QString::fromStdString(" s"));

    ui->statusbar->showMessage(QString::fromStdString("Allocated and rendered ") +
                               QString::number(rows * cols) +
                               QString::fromStdString(" double elements."), 2000);

}
