#ifndef ARRAYMODEL_H
#define ARRAYMODEL_H
#include<QAbstractTableModel>
#include <xtensor/xrandom.hpp>
#include <xtensor/xarray.hpp>

class ArrayModel : public QAbstractTableModel
{
    Q_OBJECT

private:
    xt::xarray<double> data_array;

public:
    ArrayModel(QObject *parent = nullptr, xt::xarray<double> array=xt::random::rand<double>({0, 0}));
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
    bool setArray(xt::xarray<double> array);
};

#endif // ARRAYMODEL_H
