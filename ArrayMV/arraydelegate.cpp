#include "arraydelegate.h"
#include <QLineEdit>
#include <QRegExpValidator>
#include <QRegExp>

ArrayDelegate::ArrayDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{
}

QWidget* ArrayDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    if(!index.isValid())
        return nullptr;

    QLineEdit *editor = new QLineEdit(parent);
    QRegExpValidator *validator = new QRegExpValidator(QRegExp("[0-9]+.?[0-9]+"), editor);
    editor->setValidator(validator);
    return editor;
}
