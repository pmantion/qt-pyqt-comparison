import sys
import time

import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow

from arraymodel import ArrayModel
from arraydelegate import DoubleArrayItemDelegate
from generated.ArrayMV_ui import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)
        self.model: ArrayModel = ArrayModel(np.random.rand(0, 0))
        self.tableView.setModel(self.model)
        self.tableView.setItemDelegate(DoubleArrayItemDelegate())
        self.tableView.setWordWrap(False)
        self.loadButton.clicked.connect(self.load_data)

    def load_data(self):
        rows = self.rowBox.value()
        cols = self.colBox.value()

        try:
            data_t_0 = time.time()
            array_data = np.random.rand(rows, cols)
            data_t_1 = time.time()

        except MemoryError as exception:
            self.statusbar.showMessage(str(exception), msecs=2000)

        render_t_0 = time.time()
        self.model.set_array(array_data)
        render_t_1 = time.time()

        self.renderLabel.setText(f'{render_t_1 - render_t_0} s')
        self.memoryLabel.setText(f'{data_t_1 - data_t_0} s')

        self.statusbar.showMessage(f'Allocated and rendered {rows * cols} double elements.', msecs=2000)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()
