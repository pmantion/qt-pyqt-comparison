import numpy as np
from PyQt5.QtCore import QAbstractTableModel, QModelIndex, QVariant
from PyQt5.QtCore import Qt
from overrides import overrides


class ArrayModel(QAbstractTableModel):
    def __init__(self, data_array: np.array, parent=None):
        super(ArrayModel, self).__init__(parent)
        self.data_array: np.array = data_array

    @overrides
    def rowCount(self, parent: QModelIndex) -> int:
        return self.data_array.shape[0]

    @overrides
    def columnCount(self, parent: QModelIndex) -> int:
        return self.data_array.shape[1]

    @overrides
    def headerData(self, section: int, orientation: Qt.Orientation, role: int) -> str:
        if role != Qt.DisplayRole:
            return QVariant()

        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return f'Column {section}'
            else:
                return f'Row {section}'

        return QVariant()

    @overrides
    def data(self, index: QModelIndex, role: int) -> str:
        if not index.isValid():
            return QVariant()

        if role == Qt.DisplayRole:
            return str(self.data_array[index.row(), index.column()])

    @overrides
    def flags(self, index: QModelIndex):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable | super(ArrayModel, self).flags(index)

    @overrides
    def setData(self, index: QModelIndex, value: str, role: int = Qt.EditRole) -> bool:
        if role == Qt.EditRole:
            self.data_array[index.row(), index.column()] = value
            return True

    def set_array(self, array: np.array):
        self.beginResetModel()
        self.data_array = array
        self.endResetModel()
