from PyQt5.QtCore import QModelIndex, QRegExp
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import QStyledItemDelegate, QWidget, QStyleOptionViewItem, QLineEdit
from overrides import overrides


class DoubleArrayItemDelegate(QStyledItemDelegate):
    @overrides
    def createEditor(self, parent: QWidget, option: QStyleOptionViewItem, index: QModelIndex) -> QWidget:
        if not index.isValid():
            return

        editor = QLineEdit(parent)
        validator = QRegExpValidator(QRegExp(r"[0-9]+\.?[0-9]+"), editor)
        editor.setValidator(validator)
        return editor
