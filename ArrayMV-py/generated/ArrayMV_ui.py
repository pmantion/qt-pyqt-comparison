# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ArrayMV.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(746, 542)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tableView = QtWidgets.QTableView(self.centralwidget)
        self.tableView.setObjectName("tableView")
        self.gridLayout.addWidget(self.tableView, 0, 0, 1, 3)
        self.frame_2 = QtWidgets.QFrame(self.centralwidget)
        self.frame_2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_2.setObjectName("frame_2")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.frame_2)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.label = QtWidgets.QLabel(self.frame_2)
        self.label.setObjectName("label")
        self.gridLayout_3.addWidget(self.label, 0, 0, 1, 1)
        self.label_2 = QtWidgets.QLabel(self.frame_2)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 1, 0, 1, 1)
        self.renderLabel = QtWidgets.QLabel(self.frame_2)
        self.renderLabel.setText("")
        self.renderLabel.setObjectName("renderLabel")
        self.gridLayout_3.addWidget(self.renderLabel, 0, 1, 1, 1)
        self.memoryLabel = QtWidgets.QLabel(self.frame_2)
        self.memoryLabel.setText("")
        self.memoryLabel.setObjectName("memoryLabel")
        self.gridLayout_3.addWidget(self.memoryLabel, 1, 1, 1, 1)
        self.gridLayout.addWidget(self.frame_2, 1, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(28, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 1, 1, 1)
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.rowBox = QtWidgets.QSpinBox(self.frame)
        self.rowBox.setAlignment(QtCore.Qt.AlignCenter)
        self.rowBox.setMaximum(100000)
        self.rowBox.setObjectName("rowBox")
        self.gridLayout_2.addWidget(self.rowBox, 0, 0, 1, 1)
        self.colBox = QtWidgets.QSpinBox(self.frame)
        self.colBox.setAlignment(QtCore.Qt.AlignCenter)
        self.colBox.setButtonSymbols(QtWidgets.QAbstractSpinBox.UpDownArrows)
        self.colBox.setSuffix("")
        self.colBox.setMaximum(100000)
        self.colBox.setObjectName("colBox")
        self.gridLayout_2.addWidget(self.colBox, 0, 1, 1, 1)
        self.loadButton = QtWidgets.QPushButton(self.frame)
        self.loadButton.setObjectName("loadButton")
        self.gridLayout_2.addWidget(self.loadButton, 0, 2, 1, 1)
        self.gridLayout.addWidget(self.frame, 1, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 746, 20))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "[POC] Array Editor"))
        self.label.setText(_translate("MainWindow", "Time taken to refresh view:"))
        self.label_2.setText(_translate("MainWindow", "Time Taken to allocate memory:"))
        self.rowBox.setPrefix(_translate("MainWindow", "Rows: "))
        self.colBox.setPrefix(_translate("MainWindow", "Columns: "))
        self.loadButton.setText(_translate("MainWindow", "Load Data"))

